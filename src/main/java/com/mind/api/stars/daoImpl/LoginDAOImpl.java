package com.mind.api.stars.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mind.api.stars.dao.LoginDAO;
import com.mind.api.stars.bean.UserInfo;
import com.mind.api.stars.dto.LoginDTO;
import com.mind.api.stars.global.BaseDAO;
import com.mind.api.stars.utility.AppUtility;

@Repository
public class LoginDAOImpl extends BaseDAO implements LoginDAO {

	public LoginDAOImpl() {}
	
	@Override
	public String authenticateLoginUser(LoginDTO loginDTO) {
		System.out.println(loginDTO.getUserName());
		System.out.println(loginDTO.getPassword());
		loginDTO.setPassword(AppUtility.decryptInDecimal(loginDTO.getPassword()));
		String sql="Select username, pin from m_userinfo where username=? and pin=?";
		List<UserInfo> user = jdbcTemplate.query(sql, new Object[]{loginDTO.getUserName(),loginDTO.getPassword()},new UserInfoMapper());
		if(user!=null && !user.isEmpty()) {
			return "Success";
		}
		
		return "Failure";
	}
	
	private final class UserInfoMapper implements RowMapper<UserInfo> {

		@Override
		public UserInfo mapRow(ResultSet rs, int arg1) throws SQLException {
			UserInfo user = new UserInfo();
			user.setUserName(rs.getString("username"));
			user.setPassword(rs.getString("pin"));
			return null;
		}
		
	}

}
