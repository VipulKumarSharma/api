package com.mind.api.stars.configuration;

import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

@Configuration
@ConfigurationProperties("spring.datasource")
public class ApplicationDataSource {

	@Bean
	DataSource dataSource() throws SQLException {
		SQLServerDataSource dataSource = new SQLServerDataSource();
		dataSource.setUser(userName);
		dataSource.setPassword(password);
		dataSource.setURL(url);
		return dataSource;
	}
	
	private String userName;
	private String password;
	private String url;
	private String showSql;

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getShowSql() {
		return showSql;
	}
	public void setShowSql(String showSql) {
		this.showSql = showSql;
	}

}
