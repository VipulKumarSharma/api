package com.mind.api.stars.logic;

import com.mind.api.stars.dto.LoginDTO;

public interface LoginLogic {

	public String authenticateLoginUser(LoginDTO loginDTO);
}
