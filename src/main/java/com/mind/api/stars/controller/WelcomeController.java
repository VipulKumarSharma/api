package com.mind.api.stars.controller;

import javax.annotation.Resource;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mind.api.stars.logic.LoginLogic;
import com.mind.api.stars.dto.LoginDTO;

@RestController
@CrossOrigin("*")
public class WelcomeController implements ErrorController {

	@Resource
	private LoginLogic loginLogic;
	
	@RequestMapping(value="/welcome")
	public String welcome(@RequestBody LoginDTO loginDTO) {
		System.out.println(" name: "+loginDTO.getUserName());
		System.out.println(" password: "+loginDTO.getPassword());
		String response = loginLogic.authenticateLoginUser(loginDTO);
		
		if(response.equalsIgnoreCase("success")) {
			return "Welcome! "+loginDTO.getUserName();
		} else {
			return response;
		}
	}

	@RequestMapping(value="/error")
	public String error() {
		return "Error! in Mind Services";
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}
}
