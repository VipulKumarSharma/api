package com.mind.api.stars.utility;

public class AppUtility {

	public static String encrypt(String oldPassword) {
		int len = oldPassword.length();
		String newPassword = "";
		int temp[] = new int[len];
		int i = 0;
		int ran = (int) (Math.floor(Math.random() * 256) + 1);
		int ch;
		for (i = 0; i < len; i++) {
			ch = oldPassword.charAt(i);
			temp[i] = Integer.parseInt("" + ch);
			if ((temp[i] >= 0) && (temp[i] < 64))
				temp[i] += 128;
			else if ((temp[i] >= 64) && (temp[i] < 128))
				temp[i] += 128;
			else if ((temp[i] >= 128) && (temp[i] < 192))
				temp[i] -= 128;
			else if ((temp[i] >= 192) && (temp[i] < 256))
				temp[i] -= 128;

			temp[i] += ran;
			newPassword += convertToBoolean(temp[i]);
		}
		newPassword += convertToBoolean(ran);
		return newPassword;
	}

	public static String convertToBoolean(int intcode) {
		int num = intcode;
		int quot;
		int div = 2;
		int rem;
		int temp[] = new int[9];
		int temp2[] = new int[9];
		int len = temp.length;
		String str = "";
		int k = 0;
		for (k = 0; num != 0; k++) {
			quot = num / div;
			rem = num % div;
			temp[k] = rem;
			num = quot;
		}

		for (int i = k; i < len; i++)
			temp[i] = 0;

		for (int i = len - 1, j = 0; i >= 0; i--, j++) {
			temp2[j] = temp[i];
		}

		for (int i = 0; i < len; i++)
			str += temp2[i];
		return str;
	}

	public static String decrypt(String password) {
		int len = password.length();
		String oldPassword = "";
		int pwdLen = ((len / 9) - 1);
		int temp[] = new int[pwdLen];
		int k = 0;
		int ran;
		char p;
		String strbooleantemp = "";
		String strbooleanRan = password.substring(len - 9);
		ran = convertToDecimal(strbooleanRan);

		for (k = 0; k < (pwdLen); k++) {
			strbooleantemp = password.substring((9 * k), 9 * (k + 1));
			temp[k] = (convertToDecimal(strbooleantemp) - ran);

			if ((temp[k] >= 0) && (temp[k] < 64)) {
				temp[k] += 128;
			} else if ((temp[k] >= 64) && (temp[k] < 128)) {
				temp[k] += 128;
			} else if ((temp[k] >= 128) && (temp[k] < 192)) {
				temp[k] -= 128;
			} else if ((temp[k] >= 192) && (temp[k] < 256)) {
				temp[k] -= 128;
			}
			p = (char) temp[k];
			oldPassword += p;
		}
		return oldPassword;
	}

	public static int convertToDecimal(String strbooleancode) {
		int i = 0;
		int j = 0;
		int result = 0;
		int base = 2;
		char temp = '-';
		for (i = 0, j = 8; i < 9; i++, j--) {
			temp = strbooleancode.charAt(i);
			result += Math.pow(base, j) * Integer.parseInt("" + temp);
		}
		return result;
	}

	public static String decryptInDecimal(String password) {
		int len = password.length();
		String oldPassword = "";
		int pwdLen = ((len / 9) - 1);
		int temp[] = new int[pwdLen];
		int k = 0;
		int ran;

		String strbooleantemp = "";
		String strbooleanRan = password.substring(len - 9);
		ran = convertToDecimal(strbooleanRan);

		for (k = 0; k < (pwdLen); k++) {
			strbooleantemp = password.substring((9 * k), 9 * (k + 1));
			temp[k] = (convertToDecimal(strbooleantemp) - ran);
			oldPassword += temp[k] + "";
		}
		return oldPassword;
	}

	public static String decryptFromDecimalToString(String password) {
		int len = password.length();
		String oldPassword = "";
		int pwdLen = (len / 3);
		int temp[] = new int[pwdLen];
		int k = 0;
		char p;
		String strbooleantemp = "";
		for (k = 0; k < (pwdLen); k++) {
			strbooleantemp = password.substring((3 * k), 3 * (k + 1));
			temp[k] = Integer.parseInt(strbooleantemp);

			if ((temp[k] >= 0) && (temp[k] < 64)) {
				temp[k] += 128;
			} else if ((temp[k] >= 64) && (temp[k] < 128)) {
				temp[k] += 128;
			} else if ((temp[k] >= 128) && (temp[k] < 192)) {
				temp[k] -= 128;
			} else if ((temp[k] >= 192) && (temp[k] < 256)) {
				temp[k] -= 128;
			}

			p = (char) temp[k];
			oldPassword += p;
		}
		return oldPassword;
	}

}
