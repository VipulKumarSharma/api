package com.mind.api.stars.global;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;

public abstract class BaseDAO {

	@Resource 
	public JdbcTemplate jdbcTemplate;
}
