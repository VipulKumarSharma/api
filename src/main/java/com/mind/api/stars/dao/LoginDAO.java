package com.mind.api.stars.dao;

import com.mind.api.stars.dto.LoginDTO;

public interface LoginDAO {

	public String authenticateLoginUser(LoginDTO loginDTO);
}
