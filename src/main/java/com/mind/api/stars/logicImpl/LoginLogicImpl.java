package com.mind.api.stars.logicImpl;

import javax.annotation.Resource;
import org.springframework.stereotype.Component;

import com.mind.api.stars.dao.LoginDAO;
import com.mind.api.stars.logic.LoginLogic;
import com.mind.api.stars.dto.LoginDTO;
import com.mind.api.stars.utility.AppUtility;

@Component
public class LoginLogicImpl implements LoginLogic {
	
	@Resource
	private LoginDAO loginDAO;

	@Override
	public String authenticateLoginUser(LoginDTO loginDTO) {
		loginDTO.setPassword(AppUtility.encrypt(loginDTO.getPassword()));
		String response = loginDAO.authenticateLoginUser(loginDTO);
		return response;
	}

}
