package com.mind.api.stars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@SuppressWarnings("deprecation")

@SpringBootApplication
@ComponentScan({"com.mind.api.stars"})
@EnableWebMvc
public class StarsApiApplication extends WebMvcConfigurerAdapter {

	public static void main(String[] commandLineArguments) {
		SpringApplication.run(StarsApiApplication.class, commandLineArguments);
		
		System.out.println("\n***************************************************************************");
		System.out.println("STARS API started successfully...");
		System.out.println("***************************************************************************\n");
	}
	
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
	
	@Bean
	public ViewResolver internalResourceViewResolver() {
	    InternalResourceViewResolver bean = new InternalResourceViewResolver();
	    bean.setViewClass(JstlView.class);
	    bean.setPrefix("/WEB-INF/views/");
	    bean.setSuffix(".html");
	    return bean;
	}
}
